#include <stdio.h>
#include <unistd.h> //https://man7.org/linux/man-pages/man2/pause.2.html
#include <signal.h> //https://www.man7.org/linux/man-pages/man0/signal.h.0p.html

int main(){
    while(1){
        printf("In esecuzione..\n");
        pause();
    }
    return 0;
}